"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _semanticUiReact = require("semantic-ui-react");

var _routes = require("../routes");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _jsxFileName = "/Users/nyanbu/Shoe/Programming/Solidity,\u3000React, Truffle/kickstart/components/Header.js";

exports.default = function () {
  return (
    //画面上の余白を作るため、カスタムCSSを定義。
    _react2.default.createElement(_semanticUiReact.Menu, { style: { marginTop: "10px" }, __source: {
        fileName: _jsxFileName,
        lineNumber: 8
      }
    }, _react2.default.createElement(_routes.Link, { route: "/", __source: {
        fileName: _jsxFileName,
        lineNumber: 9
      }
    }, _react2.default.createElement("a", { className: "item", __source: {
        fileName: _jsxFileName,
        lineNumber: 10
      }
    }, "CrowdCoin")), _react2.default.createElement(_semanticUiReact.Menu.Menu, { position: "right", __source: {
        fileName: _jsxFileName,
        lineNumber: 13
      }
    }, _react2.default.createElement(_routes.Link, { route: "/", __source: {
        fileName: _jsxFileName,
        lineNumber: 14
      }
    }, _react2.default.createElement("a", { className: "item", __source: {
        fileName: _jsxFileName,
        lineNumber: 15
      }
    }, "Campaigns")), _react2.default.createElement(_routes.Link, { route: "/campaigns/new", __source: {
        fileName: _jsxFileName,
        lineNumber: 18
      }
    }, _react2.default.createElement("a", { className: "item", __source: {
        fileName: _jsxFileName,
        lineNumber: 19
      }
    }, "+"))))
  );
};

/*

* {{}} ==> JSXにObjectを渡す場合は{}を2個渡す。1個めはJSを書くことを宣言、2個めはObject。

*{ Link } と <Menu.item> はクラッシュするので、一緒に書いてはいけない。今回は<Menu.item>を消す。

* <a className="item">CrowdCoin</a> ==> aタグで囲うのは慣習。item classはsemanticが用意してあるクラスで見栄えが良くなる。

*/
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvSGVhZGVyLmpzIl0sIm5hbWVzIjpbIlJlYWN0IiwiTWVudSIsIkxpbmsiLCJtYXJnaW5Ub3AiXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLEFBQU87Ozs7QUFDUCxBQUFTOztBQUNULEFBQVMsQUFBWSxBQUVyQjs7Ozs7O2tCQUFlLFlBQU0sQUFDbkI7QUFDRTtBQUNBO29CQUFBLEFBQUMsdUNBQUssT0FBTyxFQUFFLFdBQWYsQUFBYSxBQUFhO2tCQUExQjtvQkFBQSxBQUNFO0FBREY7dUJBQ0UsQUFBQyw4QkFBSyxPQUFOLEFBQVk7a0JBQVo7b0JBQUEsQUFDRTtBQURGO3VCQUNFLGNBQUEsT0FBRyxXQUFILEFBQWE7a0JBQWI7b0JBQUE7QUFBQTtPQUZKLEFBQ0UsQUFDRSxBQUdGLCtCQUFDLGNBQUQsc0JBQUEsQUFBTSxRQUFLLFVBQVgsQUFBb0I7a0JBQXBCO29CQUFBLEFBQ0U7QUFERjt1QkFDRSxBQUFDLDhCQUFLLE9BQU4sQUFBWTtrQkFBWjtvQkFBQSxBQUNFO0FBREY7dUJBQ0UsY0FBQSxPQUFHLFdBQUgsQUFBYTtrQkFBYjtvQkFBQTtBQUFBO09BRkosQUFDRSxBQUNFLEFBR0YsK0JBQUEsQUFBQyw4QkFBSyxPQUFOLEFBQVk7a0JBQVo7b0JBQUEsQUFDRTtBQURGO3VCQUNFLGNBQUEsT0FBRyxXQUFILEFBQWE7a0JBQWI7b0JBQUE7QUFBQTtPQWJSLEFBRUUsQUFLRSxBQUtFLEFBQ0UsQUFLVDs7QUFuQkQ7O0FBcUJBIiwiZmlsZSI6IkhlYWRlci5qcyIsInNvdXJjZVJvb3QiOiIvVXNlcnMvbnlhbmJ1L1Nob2UvUHJvZ3JhbW1pbmcvU29saWRpdHks44CAUmVhY3QsIFRydWZmbGUva2lja3N0YXJ0In0=