// require('next-routes')までがファンクション名、後ろの()が引数
const routes = require("next-routes")();

// 「:」はワイルドカードで、addressは特に意味ない。名前だけ。
// 二次引数はRenderする構成を設計したファイル。
//この機能を引っ張る為に、他ファイルでimport { Link }を使う。
routes
  .add("/campaigns/new", "/campaigns/new")
  .add("/campaigns/:address", "/campaigns/show")
  .add("/campaigns/:address/requests", "campaigns/requests/index")
  .add("/campaigns/:address/requests/new", "campaigns/requests/new");

module.exports = routes;
