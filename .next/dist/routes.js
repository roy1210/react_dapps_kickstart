"use strict";

// require('next-routes')までがファンクション名、後ろの()が引数
var routes = require("next-routes")();

// 「:」はワイルドカードで、addressは特に意味ない。名前だけ。
// 二次引数はRenderする構成を設計したファイル。
//この機能を引っ張る為に、他ファイルでimport { Link }を使う。
routes.add("/campaigns/new", "/campaigns/new").add("/campaigns/:address", "/campaigns/show").add("/campaigns/:address/requests", "campaigns/requests/index").add("/campaigns/:address/requests/new", "campaigns/requests/new");

module.exports = routes;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInJvdXRlcy5qcyJdLCJuYW1lcyI6WyJyb3V0ZXMiLCJyZXF1aXJlIiwiYWRkIiwibW9kdWxlIiwiZXhwb3J0cyJdLCJtYXBwaW5ncyI6Ijs7QUFBQTtBQUNBLElBQU0sU0FBUyxBQUFmOztBQUVBO0FBQ0E7QUFDQTtBQUNBLE9BQ0csQUFESCxJQUNPLEFBRFAsa0JBQ3lCLEFBRHpCLGtCQUVHLEFBRkgsSUFFTyxBQUZQLHVCQUU4QixBQUY5QixtQkFHRyxBQUhILElBR08sQUFIUCxnQ0FHdUMsQUFIdkMsNEJBSUcsQUFKSCxJQUlPLEFBSlAsb0NBSTJDLEFBSjNDOztBQU1BLE9BQU8sQUFBUCxVQUFpQixBQUFqQiIsImZpbGUiOiJyb3V0ZXMuanMiLCJzb3VyY2VSb290IjoiL1VzZXJzL255YW5idS9TaG9lL1Byb2dyYW1taW5nL1NvbGlkaXR5LOOAgFJlYWN0LCBUcnVmZmxlL2tpY2tzdGFydCJ9