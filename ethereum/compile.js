const path = require("path");
const solc = require("solc"); //solidity compiler
const fs = require("fs-extra");

const buildPath = path.resolve(__dirname, "build");
fs.removeSync(buildPath); // 1.

const campaignPath = path.resolve(__dirname, "contracts", "Campaign.sol");

const source = fs.readFileSync(campaignPath, "utf8"); // 2.

const output = solc.compile(source, 1).contracts; //3.

fs.ensureDirSync(buildPath); //4.

//output内のオブジェクトのプロパティでJsonファイルを作る
//console.log(output)すればわかるが、contractのKeyの先頭には「:」がある。見づらいから、取り除く。
for (let contract in output) {
  fs.outputJsonSync(
    path.resolve(buildPath, contract.replace(":", "") + ".json"),
    output[contract]
  );
}

/*

* fs: file system. It gives us access to the file system on our local computer.

* fs-extra: extra moduleがデフォルトで備わってる。便利キット。

* Compile work-flow
1. Delete entire 'build' folder.
2. Read 'Campaign.sol' from the 'contracts' folder.
3. Compile both contracts with solidity compiler.
4. Write output to the 'build' directory.

* removeSync:fs-extraにあるメゾッド

*ensureDirSync: fs-extraにあるメゾッド
ensure directory, if not exit, will create it for us.

* オブジェクトのプロパティを列挙できる

*/
