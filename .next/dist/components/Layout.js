"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _semanticUiReact = require("semantic-ui-react");

var _Header = require("./Header");

var _Header2 = _interopRequireDefault(_Header);

var _head = require("next/dist/lib/head.js");

var _head2 = _interopRequireDefault(_head);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _jsxFileName = "/Users/nyanbu/Shoe/Programming/Solidity,\u3000React, Truffle/kickstart/components/Layout.js";

exports.default = function (props) {
  return _react2.default.createElement(_semanticUiReact.Container, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8
    }
  }, _react2.default.createElement(_head2.default, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9
    }
  }, _react2.default.createElement("link", {
    rel: "stylesheet",
    href: "//cdn.jsdelivr.net/npm/semantic-ui@2.4.1/dist/semantic.min.css",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10
    }
  })), _react2.default.createElement(_Header2.default, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16
    }
  }), props.children);
};

/*

* {props.children}: 他のPagesにて<Layout> </Layout> の中に入っているJSXコードがChildrenプロパティ化する。

* divをContainerに置き換える。Containerならサイズのアレンジができる。コンテナーに入れたイメージ。divのままだと、画面に余白が無いが、containerだと左右に余白ができる。

* import Head from "next/head"; Nextのヘルパー。<head></head>の中に入れたコードを、HTML上でのhead tag (header) に定義する。


*/
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvTGF5b3V0LmpzIl0sIm5hbWVzIjpbIlJlYWN0IiwiQ29udGFpbmVyIiwiSGVhZGVyIiwiSGVhZCIsInByb3BzIiwiY2hpbGRyZW4iXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLEFBQU87Ozs7QUFDUCxBQUFTOztBQUNULEFBQU8sQUFBWTs7OztBQUNuQixBQUFPLEFBRVA7Ozs7Ozs7O2tCQUFlLGlCQUFTLEFBQ3RCO3lCQUNFLEFBQUM7O2dCQUFEO2tCQUFBLEFBQ0U7QUFERjtBQUFBLEdBQUEsa0JBQ0UsQUFBQzs7Z0JBQUQ7a0JBQUEsQUFDRTtBQURGO0FBQUE7U0FDRSxBQUNNLEFBQ0o7VUFGRixBQUVPOztnQkFGUDtrQkFGSixBQUNFLEFBQ0UsQUFNRjtBQU5FO0FBQ0UsdUJBS0osQUFBQzs7Z0JBQUQ7a0JBUkYsQUFRRSxBQUNDO0FBREQ7QUFBQSxZQVRKLEFBQ0UsQUFTUyxBQUdaO0FBZEQ7O0FBZ0JBIiwiZmlsZSI6IkxheW91dC5qcyIsInNvdXJjZVJvb3QiOiIvVXNlcnMvbnlhbmJ1L1Nob2UvUHJvZ3JhbW1pbmcvU29saWRpdHks44CAUmVhY3QsIFRydWZmbGUva2lja3N0YXJ0In0=