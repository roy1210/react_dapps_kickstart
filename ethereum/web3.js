import Web3 from "web3";

//const web3 = new Web3(window.web3.currentProvider);
let web3;

if (typeof window !== "undefined" && window.web3 !== "undefined") {
  // typeof windowがundefinedじゃない場合:
  // We are in the browser and metamask is running.
  web3 = new Web3(window.web3.currentProvider);
} else {
  // We are on the server *OR* the user is not running metamask.
  const provider = new Web3.providers.HttpProvider(
    "https://rinkeby.infura.io/v3/085912395fc74e3cbad791588d71c376"
  );
  web3 = new Web3(provider);
}

export default web3;
