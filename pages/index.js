import React, { Component } from "react";
import { Card, Button } from "semantic-ui-react";
import factory from "../ethereum/factory";
import Layout from "../components/Layout";
import { Link } from "../routes";

class CampaignIndex extends Component {
  static async getInitialProps() {
    const campaigns = await factory.methods.getDeployedCampaigns().call();

    return { campaigns }; // =={ campaigns:campaigns }
  }

  renderCampaigns() {
    //address: ContractのDeployしたアドレス。
    const items = this.props.campaigns.map(address => {
      return {
        header: address,
        description: (
          <Link route={`/campaigns/${address}`}>
            <a>View Campaign</a>
          </Link>
        ),
        fluid: true //横一杯まで伸ばす。
      };
    });

    return <Card.Group items={items} />;
  }

  render() {
    return (
      <Layout>
        <div>
          <h3>Open Campaigns</h3>

          <Link route="/campaigns/new">
            <a>
              <Button
                floated="right"
                content="Create Campaign"
                icon="add circle"
                primary // == primary={true} 背景色: 青
              />
            </a>
          </Link>
          {this.renderCampaigns()}
        </div>
      </Layout>
    );
  }
}

export default CampaignIndex;

/*

* static: Next.jsで使う。
CampaignIndexにてまずgetInitialPropsだけrenderする。(initial data) 他のデータは考慮せず。（一気に全部のコードを走ると遅くなる。）
また、StaticはDefines a class function
Next.jsはgetInitialPropsというメソッドを定義することで、そのメソッドの返り値をコンポーネントのpropsを経由して渡すことができます
Lifetime sycle?

*props: 親から渡されたデータ。this とあわせて、「ここで、渡された、何か」のイメージ。

* getInitialProps ==> propsは親から渡されたデータのイメージ。一番最初のPropsは渡してくれる対象の指定が難しいから、getInitialPropsとする。最初のPropsを意識する。

* <Link>タグの中に<a>タグを入れることにより、
対象となるターゲットに右クリックをすると、「新しいタブで開く」等のオプションを表示することができる。

*/
