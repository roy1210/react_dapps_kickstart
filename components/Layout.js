import React from "react";
import { Container } from "semantic-ui-react";
import Header from "./Header";
import Head from "next/head";

export default props => {
  return (
    <Container>
      <Head>
        <link
          rel="stylesheet"
          href="//cdn.jsdelivr.net/npm/semantic-ui@2.4.1/dist/semantic.min.css"
        />
      </Head>

      <Header />
      {props.children}
    </Container>
  );
};

/*

* {props.children}: 他のPagesにて<Layout> </Layout> の中に入っているJSXコードがChildrenプロパティ化する。

* divをContainerに置き換える。Containerならサイズのアレンジができる。コンテナーに入れたイメージ。divのままだと、画面に余白が無いが、containerだと左右に余白ができる。

* import Head from "next/head"; Nextのヘルパー。<head></head>の中に入れたコードを、HTML上でのhead tag (header) に定義する。


*/
