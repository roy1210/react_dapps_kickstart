pragma solidity ^0.4.17;

contract CampaignFactory {
  address[] public deployedCampaigns;

  function createCampaign(uint minimum) public {
    address newCampaign = new Campaign(minimum, msg.sender);
    deployedCampaigns.push(newCampaign);
  }

  function getDeployedCampaigns()public view returns (address[]) {
    return deployedCampaigns;
  }
}

contract Campaign {
  struct Request {
    // JSは「,」を後ろにつけるが、Solidityは「；」で区切る。
    string description;
    uint value;
    address recipient;
    bool complete;
    uint approvalCount;
    mapping (address => bool) approvals;
  }

  modifier restricted(){
    require(msg.sender == manager);
    _; //ここにオリジナルのFunctionがコピペされるイメージ。
  }

  // ↓Variables

  // Requestはタイプで、structと同じ大文字。structに繋がる。 Storage、DBのイメージ。memoryでは無い。memoryはrequestsにpushする前までのnewRequest。
  Request [] public requests;
  address public manager;
  uint public minimumContribution;

  // address[] public approvers; For loopでT/Fを探すコードはガスの消費が多いから、mapで代替え。

  // True を探し出す。Key (A/C#)とvalue (T/F) で。
  // A/Cを配列に入れるのではなく、T/Fを配列化する。
  mapping (address => bool) public approvers;
  uint public approversCount;

  // ↓Functions

  function Campaign(uint minimum, address creator)public {
    manager = creator; //msg.sender
    minimumContribution = minimum;
  }

  function contribute()public payable {
    require(msg.value > minimumContribution);

    // approvers.push(msg.sender);
    // mappingでpushされたイメージ。そこにtrueを付与している。
    approvers[msg.sender] = true;
    approversCount++;
  }

  function createRequest(string description, uint value, address recipient) public restricted {

    Request memory newRequest = Request({
      description: description,
      value: value,
      recipient: recipient,
      complete: false,
      approvalCount: 0
    });

    requests.push(newRequest);
  }

  function approveRequest(uint index) public{

    // storageにアクセス。requestは自由に名前を決めれるはず。
    Request storage request = requests[index];

    // Mappingでdonateしたかの確認。Trueで無ければ、ここで終了。
    require(approvers[msg.sender]);
    // すでにVoteしたかどうかの確認。二重投票を防ぐ。
    require(!request.approvals[msg.sender]);

    request.approvals[msg.sender] = true;
    request.approvalCount++;
  }

  function finalizeRequest(uint index) public restricted {
    Request storage request = requests[index];

    require (request.approvalCount > (approversCount / 2));
    require(!request.complete);

    request.recipient.transfer(request.value);
    request.complete = true;
  }

  function getSummary() public view returns(
    uint,uint,uint,uint,address
  ) {
    return(
      minimumContribution,
      this.balance,// 残高
      requests.length,
      approversCount,
      manager
    );
  }

  function getRequestsCount() public view returns(uint){
    return requests.length;
  }
}

/*
* contribute: likes "enter"
* Map: Key and Value
* mapping: look up
* Struct: Key and Value, able to combine different type like string and unit. image as Class

* structはグローバル変数ではなく、destinationのイメージ。

* Storage Or Memory
Storage: Struct 例 ==> Request [] requests
Mamory: 引数 例 ==> function Campaign(uint minimum)public
難しいトピックなので、復習の際は「More on Storage vs Memory」を参照。

*function createRequest
1. memoryと設定する。newRequestはrequests (storage)にpushするまでの役割だから、memoryと設定してあげる必要がある。
2. Request(description, value, recipient, false) も同じ意味だが、上の Key: Value の書き方の方が良い。なぜならStructの順番が変わったら、引数もあわせて変えないといけないから。

3. mapping (address => bool) approvalsはreference type. Valueじゃないから、デフォルト設定で入れなくて良い。

*このContractではYes(True)のみをMappingしていて、Noについては集計知ていない。ロジックとしては、Trueを過半数以上とれれば、採択できるので、設計ではNoの集計を行う必要はない。

*/
