import React, { Component } from "react";
import { Form, Button, Input, Message } from "semantic-ui-react";
import { Router } from "../../routes";
import Layout from "../../components/Layout";

//metamaskよりManagerになるA/Cを参照するため、web3が必要になる。
import web3 from "../../ethereum/web3";
import factory from "../../ethereum/factory";

class CampaignNew extends Component {
  // Set the default value.
  // Value set as string even though expect number input
  state = {
    minimumContribution: "",
    errorMessage: "",
    loading: false
  };

  onSubmit = async event => {
    //DefaultだとサーバーにSubmitするが、このBehaviourを止める
    event.preventDefault();

    this.setState({ loading: true, errorMessage: "" });
    try {
      const accounts = await web3.eth.getAccounts();
      await factory.methods
        .createCampaign(this.state.minimumContribution)
        .send({
          from: accounts[0]
        });

      // CampaignNewがエラーなく進められたら、index(Home)に動く
      Router.pushRoute("/");
    } catch (err) {
      this.setState({ errorMessage: err.message });
    }
    this.setState({ loading: false });
  };

  render() {
    return (
      <Layout>
        <h3>Create a Campaign</h3>

        {/* Binding "this" to onSubmit */}
        {/* Error Messageは表示されるが、Stateに書いたとおり、デフォルトを空欄にすることで、エラーが起きない場合は表示しないようにしている */}
        <Form onSubmit={this.onSubmit} error={!!this.state.errorMessage}>
          <Form.Field>
            <label>Minimum Contribution</label>
            <Input
              label="wei"
              labelPosition="right"
              value={this.state.minimumContribution}
              onChange={event =>
                this.setState({ minimumContribution: event.target.value })
              }
            />
          </Form.Field>

          <Message error header="Oops!!" content={this.state.errorMessage} />

          <Button loading={this.state.loading} primary>
            Create!
          </Button>
        </Form>
      </Layout>
    );
  }
}

export default CampaignNew;

/*

* Reactでclassを設定する場合、Componentを継承する必要があると思う。

* error={!!this.state.errorMessage} ==> 「!!」を付け加えることで、下のエラー(Console)を回避する。
booleanをExceptされているため、!!で否定＋否定＝肯定 と応用を利かす。
>> Warning: Failed prop type: Invalid prop `error` of type `string` supplied to `Form`, expected `boolean`.

*/
