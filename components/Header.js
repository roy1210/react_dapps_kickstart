import React from "react";
import { Menu } from "semantic-ui-react";
import { Link } from "../routes";

export default () => {
  return (
    //画面上の余白を作るため、カスタムCSSを定義。
    <Menu style={{ marginTop: "10px" }}>
      <Link route="/">
        <a className="item">CrowdCoin</a>
      </Link>

      <Menu.Menu position="right">
        <Link route="/">
          <a className="item">Campaigns</a>
        </Link>

        <Link route="/campaigns/new">
          <a className="item">+</a>
        </Link>
      </Menu.Menu>
    </Menu>
  );
};

/*

* {{}} ==> JSXにObjectを渡す場合は{}を2個渡す。1個めはJSを書くことを宣言、2個めはObject。

*{ Link } と <Menu.item> はクラッシュするので、一緒に書いてはいけない。今回は<Menu.item>を消す。

* <a className="item">CrowdCoin</a> ==> aタグで囲うのは慣習。item classはsemanticが用意してあるクラスで見栄えが良くなる。

*/
