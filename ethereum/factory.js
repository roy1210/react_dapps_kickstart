import web3 from "./web3";
import CampaignFactory from "./build/CampaignFactory.json";

const instance = new web3.eth.Contract(
  JSON.parse(CampaignFactory.interface),
  "0x28Af229395a1fCd636D0e8Bb0Af71083a7eC7212"
);

export default instance;

/*

* import web3 from "./web3"; 大文字のWじゃない。libraryにアクセスではなく、ローカルファイルからimportするから。

* 0x28Af229395a1fCd636D0e8Bb0Af71083a7eC7212 ==> A/C after deployed.

*/
